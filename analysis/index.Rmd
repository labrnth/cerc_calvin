---
title: Index
output: html_document
---


* [1. organize inputs](1.organize_inputs.html)
* [2.1 Monthly Presence of Negative Marginal Values](2.1.m_neg_percent.html)
* [2.2 Monthly Magnitude of Negative Marginal Values](2.2.m_neg_value.html)
* [3. Comparisons Between Marginal and Storage Parameters](3.comparisons.html)
* [4.1 Boxplots for the Marginal Parameter](4.1.boxplots_marginal.html)
* [4.2 Boxplots for the Storage Parameter](4.2.boxplots_storage.html)
* [5.1 Timeseries for the Marginal Parameter](5.1.timeseries_marginal.html)
* [5.2 Timeseries for the Storage Parameter](5.2.timeseries_storage.html)
* [5.3 Timeseries for Shasta Dam](5.3.timeseries_shasta.html)
* [6. Timeseries of Precipitation Indicies and Reservoir Conditions](6.cdec_reservoirs.html)
* [7. Spatial precipitation distribution](7.prism_precipitation.html)